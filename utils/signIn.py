import time
import datetime
import traceback

import pandas as pd
import requests
import json
import string
import random

from utils.callAPI import callAPI
import utils.globalVar as gv


def generate_random_string(length):
    return ''.join(random.sample(string.ascii_letters + string.digits, length))


class SignIn_Signup():
    def __init__(self):
        self.userId = None
        self.password = None
        self.embibe_token = None
        self.refresh_token = None
        self.email_id = None
        self.user_name = None

    def signIn(self, host, email, password, linked_profile_id=None):

        for i in range(2):
            headers = {
                'Connection': 'keep-alive',
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json;charset=UTF-8',
                'Origin': 'https://staging-fiber-web.embibe.com',
                'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            }

            if "fiberdemoms" in host:
                headers["Origin"] = "https://fiber-demo-web.embibe.com"
            elif "beta" in host or "api-prod" in host:
                headers["Origin"] = "https://beta-api.embibe.com"

            mobile = 0
            try:
                mobile = int(email)
            except Exception:
                pass

            if mobile == 0:
                payload = {"email": email, "password": password, "device_id": "1611312548234"}
            else:
                payload = {"mobile": str(mobile), "password": password, "device_id": "1611312548234"}

            url = f"/user_auth_ms/sign-in"
            response = callAPI(url, payload, method="POST", headers=headers)

            if response.status_code == 200:
                _embibe_token = response.headers["embibe-token"]
                self.refresh_token = response.headers["embibe-refresh-token"]

                headers['embibe-token'] = _embibe_token

                conn_prof_url = f"/user_auth_ms/connected_profiles"
                response = callAPI(conn_prof_url, {}, "GET", headers=headers)

                if response.status_code == 200:
                    if "linked_profiles" in response.json().get("data", {}) and len(
                            response.json().get("data", {}).get("linked_profiles", [])) > 0:
                        linked_profiles = response.json().get("data", {}).get("linked_profiles", [])
                        self.embibe_token = linked_profiles[0].get("embibe_token")
                        self.userId = linked_profiles[0].get("id")
                        self.email_id = email
                        self.user_name = linked_profiles[0].get("profile_details", {}).get("first_name")
                        if linked_profile_id is not None:
                            for linked_profile in linked_profiles:
                                if str(linked_profile.get("id")) == str(linked_profile_id) or str(linked_profile.get('profile_details', {}).get("first_name")).lower() in str(linked_profile_id).lower():
                                    self.embibe_token = linked_profile.get("embibe_token")
                                    self.userId = linked_profile.get("id")
                                    self.user_name = linked_profile.get("profile_details", {}).get("first_name")

                    else:
                        self.embibe_token = response.json().get("data", {}).get("embibe_token")
                        self.userId = response.json().get("data", {}).get("id")
                        self.email_id = email

                print("User Selected: ", self.user_name)
                return self.embibe_token
            else:
                print(response.status_code, "\t", response.text)

            time.sleep(5)

        return None


def create_new_user(host, primary_goal="kve383630", primary_exam="kve383631"):
    url = f"{host}/user_auth_ms/create_user"

    email = f"{generate_random_string(6).lower()}.achieve.nov21@embibe.com"
    payload = json.dumps({
        "email": email,
        "first_name": "Achieve",
        "last_name": "User",
        "password": "Embibe@1234",
        "primary_goal": primary_goal,
        "primary_exam": primary_exam,
        "profile_pic": "https://assets.embibe.com/production/web-assets/images/avatar/orange.png",
        "meta": {
            "test": "test"
        }
    })
    headers = {
        'Content-Type': 'application/json',
        'white-list-key': 'aUsb66BbYF6o2XcA5agG',
        'whitelist-source-redirected-url': 'https://beta.embibe.com/education-goa',
        'Origin': 'https://staging-fiber-web.embibe.com'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    if response.status_code == 201:
        # print("User Created")
        pass
    else:
        print(response.status_code)
        print(response.text)

    return email, "Embibe@1234"


def initUserMeta(_email=None, _random=False, password="Embibe@1234", userId=None):
    signInSrc = SignIn_Signup()

    df_emails = pd.read_csv("config/emails.csv")
    email_list = list(df_emails["email"].to_list())

    if not _random:
        if _email is None:
            gv.email, gv.password = create_new_user(gv.host, primary_goal=gv.primary_goal, primary_exam=gv.primary_exam)
        else:
            gv.email, gv.password = _email, password
    else:
        gv.email, gv.password = random.choice(email_list), password
        print("New user selected: ", gv.email)

    if gv.password is None:
        gv.password = password

    print(gv.email)
    signInSrc.signIn(gv.host, gv.email, gv.password, linked_profile_id=userId)
    gv.userId = signInSrc.userId
    # print(this.userId)

    gv.refresh_token = signInSrc.refresh_token
    gv.embibe_token = signInSrc.embibe_token
    gv.headers['embibe-token'] = gv.embibe_token