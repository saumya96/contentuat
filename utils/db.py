import os
import sys
from pymongo import MongoClient


def initAchieveDB(host="https://preprodms.embibe.com"):
    db = None
    if host == "https://api-prod-cf.embibe.com":
        client = MongoClient("mongodb://achieve-user:xyz123#$$!v3@175.0.96.10:27017,175.0.96.11:27017,"
                             "175.0.96.12:27017,175.0.96.13:27017/achieve_v3", connect=False)
        db = client["achieve_v3"]
    else:
        client = MongoClient("mongodb://achieve:W3R5exRO%2FqG4[pu@10.146.1.21:27017/achieve", connect=False)
        db = client["achieve"]

    this.achieve_user_session_coll = db.user_session


def initCGDB():
    client = MongoClient("mongodb://ro_dsl:EHJpUwVO2vgMuk@10.141.11.76/contentgrail")
    db = client["contentgrail"]

    this.cg_learning_objects_coll = db.learning_objects
    this.cg_learning_objects_versions_coll = db.learning_objects_versions


def nqe_question_count_collection():
    client = MongoClient("mongodb://10.141.12.55/")
    database = client["learning_interventions"]

    this.nqe_avg_ques = database.nqe_avg_ques


this = sys.modules[__name__]
this.achieve_user_session_coll = None
this.cg_learning_objects_coll = None
this.nqe_avg_ques = None
this.cg_learning_objects_versions_coll = None

initAchieveDB()
initCGDB()
nqe_question_count_collection()
