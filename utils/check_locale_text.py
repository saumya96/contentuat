import re
from bs4 import BeautifulSoup as BS


def removeMathML(text):
    start1 = "<math xmlns="
    start2 = "<math>"
    end = "</math>"
    i = 0
    while True:
        i += 1
        if i == 10:
            break

        if (text.find(start1) == -1 and text.find(start2) == -1) or text.find(end) == -1:
            break

        start = start1
        if text.find(start1) == -1 or (text.find(start2) != -1 and text.find(start2) < text.find(start1)):
            start = start2

        text = text[:text.find(start)] + text[text.find(end) + len(end):]

    return text


def get_mathml_ans(datas):
    soup = BS(datas)
    return str(soup.get_text()).replace("\n", "")


def clearText(text):
    # Removing mathml
    text = removeMathML(text)

    # Removing links
    text = re.sub(r"http\S+", "", text)
    text = get_mathml_ans(text)
    return text


def check_non_en_text(text, uni_start, uni_end, fail_thershold=35):
    text = clearText(text)

    # Removing spaces
    tt = text.replace("&nbsp;", "").replace("&#39;", "").replace("\n", "").replace("Embibe", "").replace("embibe", "")
    tt = tt.replace("sin", "").replace("cos", "").replace("tan", "").replace("  ", "")

    # print(tt)
    if type(tt) == str and len(text) < 5:
        return True

    non_hindi_chars = 0
    total_text_len = len(text)
    special_char_and_num = list(range(1, 65)) + list(range(91, 97)) + list(range(123, 128)) + [160]
    if type(tt) == str and len(tt) > 0:
        for ch in tt:
            if not (uni_start <= ord(ch) <= uni_end) and (ord(ch) not in special_char_and_num):
                # print(ord(ch))
                non_hindi_chars += 1

        failure_ratio = (non_hindi_chars * 100) / total_text_len
        # print(fail_thershold)
        if failure_ratio > fail_thershold:
            return False

        return True

    return False


def check_locale_text(text, locale, fail_thershold=35):
    if locale == "en":
        return True
    elif locale == "hi" or locale == "mr":
        return check_non_en_text(text, 2304, 2431, fail_thershold)
    elif locale == "te": # Telgu
        return check_non_en_text(text, 3072, 3199, fail_thershold)
    elif locale == "kn": #Kannada
        return check_non_en_text(text, 3200, 3327, fail_thershold)
    elif locale == "ta": # Tamil
        return check_non_en_text(text, 2944, 3071, fail_thershold)
    elif locale == "ml": # Kerala - Malayalam
        return check_non_en_text(text, 3328, 3455, fail_thershold)
    elif locale == "pa": # Punjabi - Gurmukhi
        return check_non_en_text(text, 2560, 2687, fail_thershold)
    elif locale == "as" or locale == "bn" : # Assam - Bengal
        return check_non_en_text(text, 2432, 2559, fail_thershold)
    elif locale == "gu": # Gujarat Board
        return check_non_en_text(text, 2688, 2815, fail_thershold)
    elif locale == "or": # Odisha Board
        return check_non_en_text(text, 2816, 2943, fail_thershold)
