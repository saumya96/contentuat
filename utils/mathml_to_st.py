import math
from pylatexenc.latex2text import LatexNodes2Text
from lxml import etree
from bs4 import BeautifulSoup as BS
import os


def mathml2latex_yarosh(equation):
    """ MathML to LaTeX conversion with XSLT from Vasil Yaroshevich """
    xslt_file = os.path.join('xsltml_2.0', 'mmltex.xsl')
    dom = etree.fromstring(equation)
    xslt = etree.parse(xslt_file)
    transform = etree.XSLT(xslt)
    newdom = transform(dom)
    ans = LatexNodes2Text().latex_to_text(str(newdom))
    return ans


def get_mathml_to_st(datas):
    if "<math" in datas and '</math>' in datas:
        start = '<math'
        end = '</math>'
        try:
            ans = mathml2latex_yarosh(datas[datas.find(start):datas.find(end) + len(end)])
        except Exception as e:
            print(e)
            ans = ""

        return str(get_mathml_to_st(datas[:datas.find(start)])) + ans + str(
            get_mathml_to_st(datas[datas.find(end) + len(end):]))

    soup = BS(datas, features="lxml")
    return str(soup.get_text()).replace("\n", "")
