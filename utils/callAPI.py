import json
import time
import traceback

import requests
import utils.globalVar as gv


class RequestResponse:
    def __init__(self):
        self.status_code = 504
        self.text = "Error due to timeout"


def callAPI(url, payload, method="GET", no_of_iterations=2, timeout=150, headers=None):
    if headers is None:
        headers = gv.headers

    response = None
    for i in range(no_of_iterations):
        try:
            if method == "POST":
                payload = json.dumps(payload)
                response = requests.request(method, gv.host + url, headers=headers, data=payload, timeout=timeout)
            else:
                response = requests.request(method, str(gv.host + url), headers=headers, timeout=timeout)
        except requests.Timeout:
            return RequestResponse()
        except Exception:
            print(payload, gv.host + url)
            print(traceback.format_exc())

        if response.status_code in [200, 204, 409, 410, 201]:
            return response

    print(response.status_code, response.text, payload, gv.host + url)

    if response is None or response.text is None:
        print(url)
        print(gv.headers)
        print(json.dumps(payload))

    return response


def refresh_embibe_token():
    print("\tRefreshing Embibe Token")
    url = f"{gv.host}/user_auth_ms/refresh-embibe-token"

    payload = {}
    headers = {
        'embibe-token': gv.embibe_token,
        'embibe-refresh-token': gv.refresh_token
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    if response.status_code == 200:
        gv.embibe_token = response.headers.get("embibe-token")

        print("Success")
    else:
        print("Fail: ", response.status_code)
