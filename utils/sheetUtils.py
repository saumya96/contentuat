import os

import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import gspread_dataframe as gd
import time
import traceback

# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

creds = ServiceAccountCredentials.from_json_keyfile_name('googlesheetcreds.json', scope)
client = gspread.authorize(creds)

gc = gspread.service_account(filename='googlesheetcreds.json')


def update_sheet_by_df_with_url(df, url, worksheet_name=None, worksheetId=None):
    time.sleep(5)
    for i in range(5):
        try:
            sheet = gc.open_by_url(url)
            if worksheet_name is not None:
                worksheet = sheet.worksheet(worksheet_name)
            elif worksheetId is not None:
                worksheet = sheet.get_worksheet_by_id(worksheetId)

            worksheet.clear()
            gd.set_with_dataframe(worksheet, df)
            return
        except Exception as e:
            print(traceback.format_exc())
            time.sleep(102)
            print(url, worksheet_name, worksheetId)


def get_sheet_to_df_with_url(url, worksheet_name=None, worksheetId=None):
    time.sleep(5)
    print("\t", worksheet_name, worksheetId)
    for i in range(5):
        try:
            sheet = gc.open_by_url(url)
            if worksheet_name is not None:
                worksheet = sheet.worksheet(worksheet_name)
            elif worksheetId is not None:
                worksheet = sheet.get_worksheet_by_id(worksheetId)

            return pd.DataFrame(worksheet.get_all_records())
        except Exception as e:
            print(url, worksheet_name, worksheetId)
            print(traceback.format_exc())
            time.sleep(102)
