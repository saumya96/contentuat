from __future__ import print_function
import pickle
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import time
from apiclient.http import MediaFileUpload, MediaIoBaseDownload
import os
import traceback
import mimetypes

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/drive.file']

"""Shows basic usage of the Drive v3 API.
Prints the names and ids of the first 10 files the user has access to.
"""
creds = None
# The file token.pickle stores the user's access and refresh tokens, and is
# created automatically when the authorization flow completes for the first
# time.
if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)
# If there are no (valid) credentials available, let the user log in.
if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server(port=0)
    # Save the credentials for the next run
    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)

service = build('drive', 'v3', credentials=creds)


def upload_csv_to_drive(file_name, parent_folder_id, file_to_upload):
    media = MediaFileUpload(file_to_upload,
                            mimetype='text/csv',
                            resumable=True)
    file_metadata = {
        'name': file_name,
        'parents': [parent_folder_id],
        'mimeType': 'application/vnd.google-apps.spreadsheet'
    }
    file = service.files().create(body=file_metadata,
                                  media_body=media,
                                  fields='id').execute()
    return file.get('id')


def delete_file(file_id):
    """Permanently delete a file, skipping the trash.

    Args:
    service: Drive API service instance.
    file_id: ID of the file to delete.
    """
    try:
        service.files().delete(fileId=file_id).execute()
    except Exception:
        print(traceback.format_exc())


def files_subfolder_in_main_folder(parent_folder_id):
    page_token = None
    response = service.files().list(q=f"'{parent_folder_id}' in parents",
                                    spaces='drive',
                                    fields='nextPageToken, files(id, name)',
                                    pageToken=page_token).execute()

    files = {}
    for file in response.get('files', []):
        files[file.get('name')] = file.get('id')

    return files


def upload_xlsx_to_drive(file_name, parent_folder_id, file_to_upload):
    for i in range(2):
        try:
            media = MediaFileUpload(file_to_upload,
                                    mimetype='application/vnd.ms-excel',
                                    resumable=True)
            file_metadata = {
                'name': file_name,
                'parents': [parent_folder_id],
                'mimeType': 'application/vnd.google-apps.spreadsheet'
            }
            file = service.files().create(body=file_metadata,
                                          media_body=media,
                                          fields='id').execute()
            return file.get('id')
        except Exception:
            print(traceback.format_exc())
            time.sleep(10)
            pass
