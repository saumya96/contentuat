import jwt
import datetime


def createJWT(userId, host="https://preprodms.embibe.com"):
    time_now = datetime.datetime.now()
    exp_time = time_now.timestamp() + 1000000

    payload = {
        "country": 1,
        "user_type": 1,
        "parent_user_id": 1500047034,
        "created": int(time_now.timestamp()),
        "organization_id": "1",
        "id": int(userId),
        "exp": int(exp_time),
        "deviceId": "1611312548234",
        "mobile_verification_status": True,
        "email_verification_status": True,
        "email": "1500047034_9892130801346466@embibe-user.com"
    }

    secret_key = "1df65b5faf39efdf05746acbc98600290135d9b090eba7fa51d997f275a9f139f55cf3550a8431fe39792af589c8e773ee3d20f368ce1e013be07a7ad3457968"

    if host == "https://api-prod-cf.embibe.com":
        return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoyMDAwMTE5Mzg3LCJjcmVhdGVkIjoxNjM1NDk4MDI0LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjIwMDAxMzA4MDAsImV4cCI6MTYzNjcwNzYyNCwiZGV2aWNlSWQiOiIxNjM1NDk4MDIzMDkzIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIyMDAwMTE5Mzg3Xzc1NjYyODAzMTQxNzkyNDFAZW1iaWJlLXVzZXIuY29tIn0.i5qnIuLVzoeZSa5Dev-xBGT2znMCBBMhrTQe3Rz-RSECY2bH9doWIlUSPR32ziWs5WdCUTUX5dWfEkQpHJjtDg"

    encoded = jwt.encode(payload, secret_key, algorithm="HS512")

    return encoded
