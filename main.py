import datetime

import pandas as pd
from tqdm import tqdm
import utils.db as db

from config.config import *
from utils.drive_utils import upload_csv_to_drive
from utils.sheetUtils import get_sheet_to_df_with_url, update_sheet_by_df_with_url


def main():
    df_question_list = get_sheet_to_df_with_url(QuestionIdsSheetURL, worksheetId=QuestionIdsSheetWorkSheetId)
    question_list = list(df_question_list["CG ID"])
    all_question_ids = []
    for questionId in question_list:
        all_question_ids.append(int(questionId))

    for i in range(5):
        print("Checking Part: ", i+1)

        if i == 0:
            continue

        start = int(i*len(all_question_ids)/5)
        end = int((i+1)*len(all_question_ids)/5)

        print("\nStart: ", start, "\tEnd: ", end)

        question_ids = all_question_ids[start:end]

        print(question_ids[0])

        query2 = {'id': {'$in': question_ids}}
        projection2 = {"id": 1.0, 'status': 1.0}

        cursor2 = db.cg_learning_objects_coll.find(query2, projection=projection2)
        q_count = db.cg_learning_objects_coll.count_documents(query2)
        ids = []
        count = 0
        for doc in tqdm(cursor2, total=q_count):
            count = count + 1
            if count % 60000 == 0:
                print(doc['id'])
            question2 = {}

            try:
                question2['id'] = doc['id']
                question2['status'] = doc['status']
            except:
                pass

            ids.append(question2)

        print("Start")
        query = {}
        status = ["UAT accepted", "Published UAT Accepted", "UAT Rejected", "UAT Accepted"]
        query['id'] = {'$in': question_ids}
        query["status"] = {"$in": status}
        print("Check")
        projection = {"id": 1.0, "status": 1.0, "_id": 0, "wf_info": 1.0, "updated_at": 1.0, "updated_by": 1.0,
                      "_version": 1.0,
                      'content': 1.0}
        print("Check")
        cursor = db.cg_learning_objects_versions_coll.find(query, projection=projection)

        print(type(cursor))
        question_objects = []
        count = 0
        for doc in tqdm(cursor):
            count = count + 1
            if count % 60000 == 0:
                print(doc['id'])
            question = {'id': doc['id'], 'version': doc['_version'], 'status': doc['status'],
                        'updated_at': doc['updated_at'],
                        'updated_by': doc['updated_by']}

            bmt = doc['content'].get('book_meta_tags', [])

            for entry in bmt:
                if entry.get('key') == 'Grade':
                    question['Grade'] = entry.get('value')
                if entry.get('key') == 'Subject':
                    question['Subject'] = entry.get('value')
                if entry.get('key') == 'Code':
                    question['Book Code'] = entry.get('value')

            errors = []
            for entry in doc.get('wf_info', {}).get('errors', []):
                errors.extend(entry['errors'])

            question['errors'] = errors
            question_objects.append(question)

        print("End1")

        final_data = []
        for question_meta in question_objects:
            temp_id = int(question_meta.get('id'))
            final_question = {'CG ID': question_meta.get('id'), 'Version': question_meta.get('version'),
                              'UAT Status': question_meta.get('status')}

            for id in ids:
                if id.get('id') == temp_id:
                    final_question['Current Status'] = id.get('status')

            final_question['Updated at'] = question_meta.get('updated_at')
            final_question['Updated by'] = question_meta.get('updated_by')
            final_question['Grade'] = question_meta.get('Grade')
            final_question['Subject'] = question_meta.get('Subject')
            final_question['Book Code'] = question_meta.get('Book Code')
            final_question['Errors'] = question_meta.get('errors')
            final_data.append(final_question)

        print("End2")

        df3 = pd.DataFrame(final_data)
        df3.to_csv(f"final_data part {i+1}.csv", index=False)

        now = datetime.datetime.now()
        upload_csv_to_drive(f"content_uat {str(now.day)}/{str(now.month)} part {i+1}", DriveFolderId,
                            f"final_data part {i+1}.csv")


if __name__ == '__main__':
    main()
